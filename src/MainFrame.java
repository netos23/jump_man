import jump_man.JumpMan;
import jump_man.Shape;
import jump_man.SkyCanvas;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;


public class MainFrame extends JFrame {
	public static final String WINDOW_NAME = "Sky";
	public static final int WIDTH = 1200;
	public static final int HEIGHT = 1000;

	private Canvas canvas;

	public MainFrame() {
		super(WINDOW_NAME);
		List<Shape> shapes = new ArrayList<>();

		// задание первого парашюта
		JumpMan first = JumpMan.newBuilder()
				.setMainColor(Color.RED)
				.setHeight(300)
				.setWidth(200)
				.setX(100)
				.setY(100)
				.setSlingsCount(5)
				.build();

		JumpMan second = JumpMan.newBuilder()
				.setMainColor(Color.GREEN)
				.setHeight(200)
				.setWidth(100)
				.setX(500)
				.setY(100)
				.setSlingsCount(2)
				.build();

		JumpMan third = JumpMan.newBuilder()
				.setMainColor(Color.BLUE)
				.setHeight(600)
				.setWidth(400)
				.setX(600)
				.setY(300)
				.setSlingsCount(8)
				.build();

		shapes.add(first);
		shapes.add(second);
		shapes.add(third);

		canvas = new SkyCanvas(shapes, Color.WHITE);

		add(canvas);
		setSize(WIDTH, HEIGHT);
	}

	public static void main(String[] args) {
		MainFrame mainFrame = new MainFrame();
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setVisible(true);
	}
}
