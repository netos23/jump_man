package jump_man;

import java.awt.*;
import java.util.List;

public class SkyCanvas extends Canvas {


	private final List<Shape> shapes;
	private final Color bgColor;

	public SkyCanvas(List<Shape> shapes, Color bgColor) {
		this.shapes = shapes;
		this.bgColor = bgColor;
		this.setBackground(bgColor);
	}

	@Override
	public void paint(Graphics g) {
		for(Shape shape : shapes){
			shape.draw(g);
		}
	}


	public List<Shape> getShapes() {
		return shapes;
	}

	public Color getBgColor() {
		return bgColor;
	}
}
