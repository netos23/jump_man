package jump_man;

import java.awt.Graphics;

public interface Shape {
	int getX();
	int getY();
	int getWidth();
	int getHeight();
	void draw(Graphics g);
}
