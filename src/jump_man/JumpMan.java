package jump_man;

import java.awt.*;

public class JumpMan implements Shape {
	private int slingsCount;
	private int width;
	private int height;
	private int x;
	private int y;
	private Color bg;
	private Color main;
	private Color slingsColor;

	/**
	 * Приватный конструктор, в котором задаются значения полей по умолчанию
	 */
	private JumpMan() {
		slingsCount = 3;
		width = 100;
		height = 300;
		x = 0;
		y = 0;
		bg = Color.WHITE;
		main = Color.RED;
		slingsColor = Color.BLACK;
	}

	/**
	 * Так как много параметров у конструктора, то удобно использовать билдер
	 */
	public static JumpManBuilder newBuilder() {
		return new JumpMan().new JumpManBuilder();
	}

	@Override
	public int getX() {
		return x;
	}

	@Override
	public int getY() {
		return y;
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public void draw(Graphics g) {
		// отрисовка главного купола
		g.setColor(main);
		g.fillArc(x, y, width, width, 0, 180);

		// отрисовка сегментов
		g.setColor(bg);
		int diameter = width / slingsCount;
		int tmpY = this.y + width / 2 - diameter / 2;
		for (int i = 0; i < slingsCount; i++) {
			int tmpX = this.x + diameter * i;
			g.fillArc(tmpX, tmpY, diameter, diameter, 0, 180);
		}

		// отрисовка строп
		g.setColor(slingsColor);
		int endX = x + width / 2;
		int endY = y + height;
		tmpY = this.y + width / 2 - 1;
		for (int i = 0; i < slingsCount + 1; i++) {
			int tmpX = this.x + diameter * i;
			g.drawLine(tmpX, tmpY, endX, endY);
		}
	}

	public class JumpManBuilder {
		public JumpManBuilder setSlingsCount(int count) {
			JumpMan.this.slingsCount = count;
			return this;
		}

		public JumpManBuilder setWidth(int width) {
			JumpMan.this.width = width;
			return this;
		}

		public JumpManBuilder setHeight(int height) {
			JumpMan.this.height = height;
			return this;
		}

		public JumpManBuilder setX(int x) {
			JumpMan.this.x = x;
			return this;
		}

		public JumpManBuilder setY(int y) {
			JumpMan.this.y = y;
			return this;
		}

		public JumpManBuilder setBgColor(Color color) {
			JumpMan.this.bg = color;
			return this;
		}

		public JumpManBuilder setMainColor(Color color) {
			JumpMan.this.main = color;
			return this;
		}

		public JumpManBuilder setSlingsColor(Color color) {
			JumpMan.this.slingsColor = color;
			return this;
		}

		public JumpMan build() {
			return JumpMan.this;
		}

	}
}
